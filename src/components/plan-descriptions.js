import { Text, View, StyleSheet } from "@react-pdf/renderer";

const styles = StyleSheet.create({
  paragraph: {
    fontSize: 12,
    fontWeight: "normal",
    color: "#4c4c4c",
    marginBottom: 20,
  },
  title: {
    fontSize: 12,
    fontWeight: "bold",
    color: "red",
  },
});

export default function PlanDescription({ title, content }) {
  return (
    <View>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.paragraph}>{content}</Text>
    </View>
  );
}
