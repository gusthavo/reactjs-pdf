import { Image, Text, View, StyleSheet } from "@react-pdf/renderer";
import {
  Table,
  TableHeader,
  TableCell,
  TableBody,
  DataTableCell,
} from "@david.kucsai/react-pdf-table";

const styles = StyleSheet.create({
  odontogram: {
    flex: 1,
    flexDirection: "row",
    flexGrow: 1,
    border: "1px solid black",
    marginBottom: 10,
  },
  leftColumn: {
    flexGrow: 0,
    flexShrink: 1,
    flexBasis: 200,
    justifyContent: "center",
  },
  rightColumn: {
    padding: 5,
    // width: '66%', //<- working alternative
    flexShrink: 1,
    flexGrow: 2,
  },
  title: {
    fontSize: 12,
  },
});

export default function Odontogram({ title, imageSrc }) {
  return (
    <View style={styles.odontogram}>
      <View style={styles.leftColumn}>
        <Text style={styles.title}>{title}</Text>
      </View>
      <View style={styles.rightColumn}>
        <Image src={imageSrc} />
      </View>
    </View>
  );
}
