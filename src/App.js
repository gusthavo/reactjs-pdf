import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  PDFViewer,
  Image,
  Font,
  BlobProvider,
} from "@react-pdf/renderer";

import {
  Table,
  TableHeader,
  TableCell,
  TableBody,
  DataTableCell,
} from "@david.kucsai/react-pdf-table";

import screenshot from "./img/makemeclear.png";
import PlanDescription from "./components/plan-descriptions";
import Odontogram from "./components/odontogram";

import Header from "./components/header";
import Footer from "./components/footer";

import RobotoRegular from "./fonts/Roboto-Regular.ttf";
import RobotoBold from "./fonts/Roboto-Bold.ttf";
import RobotoLight from "./fonts/Roboto-Light.ttf";

Font.register({
  family: "Roboto",
  fonts: [
    {
      src: RobotoRegular,
    },
    {
      src: RobotoBold,
      fontWeight: "bold",
    },
    {
      src: RobotoLight,
      fontWeight: "lighter",
    },
  ],
});

const styles = StyleSheet.create({
  page: {
    padding: 20,
    height: 500,
  },
  paragraph: {
    fontSize: 12,
    fontWeight: "normal",
    color: "#4c4c4c",
    marginBottom: 20,
    fontFamily: "Roboto",
  },
  title: {
    fontSize: 12,
    fontWeight: "bold",
    fontFamily: "Roboto",
  },
  table: {
    border: "1px solid black",
  },
  tableRow: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    pageBreakInside: "avoid",
    width: "100%",
  },
  tableColumn: {
    textAlign: "center",
    fontWeight: "bold",
    width: "100%",
    backgroundColor: "gray",
    color: "black",
    height: 30,
    lineHeight: 30,
  },

  tableContent: {
    textAlign: "center",
    borderBottom: "1px solid gray",
    width: "100%",
    height: 25,
    lineHeight: 25,
    color: "#000",
  },
});

const tableData = [
  {
    name: "Enxerto Teste2",
    teeth: "45",
    type: "",
    notes: "",
    value: "333",
  },
  {
    name: "Enxerto Teste2",
    teeth: "45",
    type: "",
    notes: "",
    value: "333",
  },
  {
    name: "Enxerto Teste2",
    teeth: "45",
    type: "",
    notes: "",
    value: "333",
  },
  {
    name: "Enxerto Teste2",
    teeth: "45",
    type: "",
    notes: "",
    value: "333",
  },
  {
    name: "Enxerto Teste2",
    teeth: "45",
    type: "",
    notes: "",
    value: "333",
  },
];

const MyDocument = () => (
  <Document>
    <Page size="A4" style={styles.page}>
      <Header />
      <View style={{ flex: 1 }}>
        <PlanDescription
          title="Aconselhamento Comportamental custom Numa grande parte das desordens"
          content="articulares temporomandibulares (problemas musculares e da articulação
          dos maxilares) é essencial que esteja consciente da tensão nos seus
          maxilares. Com prática será mais fácil evitar apertar e ranger os seus
          dentes durante o dia."
        />

        <Odontogram
          title="Dentição Perfeita na sua idade"
          imageSrc={screenshot}
        />

        <View>
          <Text style={styles.title}>
            Aconselhamento Comportamental custom Numa grande parte das desordens
          </Text>
          <Text style={styles.paragraph}>
            articulares temporomandibulares (problemas musculares e da
            articulação dos maxilares) é essencial que esteja consciente da
            tensão nos seus maxilares. Com prática será mais fácil evitar
            apertar e ranger os seus dentes durante o dia.
          </Text>
        </View>
        <View>
          <Text style={styles.title}>
            Aconselhamento Comportamental custom Numa grande parte das desordens
          </Text>
          <Text style={styles.paragraph}>
            articulares temporomandibulares (problemas musculares e da
            articulação dos maxilares) é essencial que esteja consciente da
            tensão nos seus maxilares. Com prática será mais fácil evitar
            apertar e ranger os seus dentes durante o dia.
          </Text>
        </View>
        <View>
          <Text style={styles.title}>
            Aconselhamento Comportamental custom Numa grande parte das desordens
          </Text>
          <Text style={styles.paragraph}>
            articulares temporomandibulares (problemas musculares e da
            articulação dos maxilares) é essencial que esteja consciente da
            tensão nos seus maxilares. Com prática será mais fácil evitar
            apertar e ranger os seus dentes durante o dia.
          </Text>
        </View>
        <View>
          <Text style={styles.title}>
            Aconselhamento Comportamental custom Numa grande parte das desordens
          </Text>
          <Text style={styles.paragraph}>
            articulares temporomandibulares (problemas musculares e da
            articulação dos maxilares) é essencial que esteja consciente da
            tensão nos seus maxilares. Com prática será mais fácil evitar
            apertar e ranger os seus dentes durante o dia.
          </Text>
        </View>
        <View>
          <Text style={styles.title}>
            Aconselhamento Comportamental custom Numa grande parte das desordens
          </Text>
          <Text style={styles.paragraph}>
            articulares temporomandibulares (problemas musculares e da
            articulação dos maxilares) é essencial que esteja consciente da
            tensão nos seus maxilares. Com prática será mais fácil evitar
            apertar e ranger os seus dentes durante o dia.
          </Text>
        </View>

        <View>
          <Text style={styles.title}>Coroa Teste</Text>
          <Text style={styles.paragraph}>
            Etiam consectetur blandit lacinia. Sed vestibulum magna turpis,
            laoreet egestas turpis fringilla sit amet. Cras tempus ipsum id
            fringilla hendrerit. Morbi velit ligula, mollis sit amet interdum
          </Text>
        </View>
        <View>
          <Text style={styles.title}>
            Comunicar com clareza é muito importante para nós.
          </Text>
        </View>

        <View style={{ marginTop: 20 }} wrap={false}>
          <Table data={tableData} style={{ display: "table-row-group" }}>
            <TableHeader style={styles.thead} textAlign={"center"}>
              <TableCell></TableCell>
              <TableCell>Dentes</TableCell>
              <TableCell>Face/Tipo</TableCell>
              <TableCell>Observações</TableCell>
              <TableCell>Valor</TableCell>
            </TableHeader>
            <TableBody>
              <DataTableCell getContent={(r) => r.name} />
              <DataTableCell getContent={(r) => r.teeth} />
              <DataTableCell getContent={(r) => r.type} />
              <DataTableCell getContent={(r) => r.notes} />
              <DataTableCell getContent={(r) => r.value} />
            </TableBody>
          </Table>
        </View>
      </View>

      <Footer />
    </Page>
    <Page size="A4" style={styles.page}>
      <View>
        <Text style={styles.title}>Plano</Text>
      </View>
      <Image src={screenshot} />
    </Page>
  </Document>
);

function App() {
  return (
    <PDFViewer style={{ width: "100%", height: 800 }}>
      <MyDocument />
    </PDFViewer>
  );
}

export default App;
